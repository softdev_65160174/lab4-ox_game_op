/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package oxgame_op;

/**
 *
 * @author Nobpharat
 */
class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player player1;
    private Player player2;
    private Player currentPlayer;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowCol(int row, int col) {
        if (table[row - 1][col - 1] == '-') {
            table[row - 1][col - 1] = currentPlayer.getSymbol();
            return true;
        }
        return false;
    }

    public void switchPlayer() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

    public boolean checkWin() {
        for (int i = 0; i < 3; i++) {
            if (checkRow(i) || checkCol(i) || checkDiagonal()) {
                saveWin();
                return true;
            }
        }
        return false;
    }

    public boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] != 'X' && table[i][j] != 'O') {
                    return false;
                }
            }
        }
        saveDraw();
        return true;
    }

    private boolean checkRow(int row) {
        for (int i = 0; i < 3; i++) {
            if (table[row][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkCol(int col) {
        for (int i = 0; i < 3; i++) {
            if (table[i][col] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkDiagonal() {
        if ((table[0][0] == currentPlayer.getSymbol() && table[0][0] == table[1][1] && table[1][1] == table[2][2])
                || (table[1][1] == currentPlayer.getSymbol() && table[1][1] == table[0][2] && table[1][1] == table[2][0])) {
            return true;
        }
        return false;
    }
    
    private void saveWin(){
        if(player1 == getCurrentPlayer()){
            player1.win();
            player2.lose();
        }else{
            player2.win();
            player1.lose();
        }
    }
    
    private void saveDraw(){
        player1.draw();
        player2.draw();
    }
    
}
