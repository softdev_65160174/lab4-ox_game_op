/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package oxgame_op;

import java.util.Scanner;

/**
 *
 * @author Nobpharat
 */
public class Game {


    private Player player2;
    private Player player1;
    private Table table;

    public Game() {
        this.player1 = new Player('X');
        this.player2 = new Player('O');
    }
    
    public void newGame() {
        this.table = new Table(player1, player2);
    }
    
   public void play() {
        showWelcome();
        while (true) {
            newGame();
            while (true) {
                showTable();
                showTurn();
                inputRowCol();
                if (table.checkWin()) {
                    showTable();
                    System.out.println(table.getCurrentPlayer().getSymbol() + " wins!");
                    break;
                }
                if (table.checkDraw()) {
                    showTable();
                    System.out.println("It's a draw!");
                    break;
                }
                table.switchPlayer();
            }
            showInfo();
            if (!isAgain()) {
                break;
            }
        }
    }

    private void showWelcome() {
        System.out.println("Welcome to XO Game");
    }

    private void showTable() {
        char[][] board = table.getTable();
       for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row,col : ");
        while (true) {
            int row = sc.nextInt();
            int col = sc.nextInt();
            if (row > 0 && row < 4 && col > 0 && col < 4) {
                if (table.getTable()[row - 1][col - 1] == '-') {
                    table.getTable()[row - 1][col - 1] = table.getCurrentPlayer().getSymbol();
                    return;
                }
            }
            System.out.print("Invalid input. Please enter row[1-3] col[1-3] : ");
        }
    }
    private boolean isAgain() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input continue (y/n)  : ");
        while (true) {
            String ans = sc.next();
            if (ans.equals("y") || ans.equals("n")) {
                if (ans.equals("y")) {
                    return true;
                } else {
                    return false;
                }
            }
            System.out.println("to start the game. Please choose y or n.");
        }
    }

    private void showInfo() {
        System.out.println(player1);
        System.out.println(player2);
    }

    
}
